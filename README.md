# Jugalux Sqlite CRUD Posts App : How to execute the project

After cloning this React Native project, you need to follow these steps to launch it on your phone:

> Steps :

* Make sure you have Node.js installed on your computer. You can check this by opening a command prompt and typing node -v. If Node.js is installed, you should see the installed version. If Node.js is not installed, you can download it from the official website: https://nodejs.org/en/ .
* Install Expo CLI by typing npm install -g expo-cli in your command prompt. This will install Expo CLI globally on your computer.
* Open your terminal at the root of the cloned project.
* Install the project dependencies by typing 'npm install' in your terminal.
* Once all the dependencies are installed, type 'expo start' in your terminal to launch the Expo development server.
* Open the Expo application on your phone. If you have not yet installed it, you can download it from the Google Play Store (Android).
* Scan the QR code displayed in your order prompt with the Expo app on your phone. This should open the app on your phone.
* If everything has been done correctly, you should see the application open on your phone and you can use it normally.

> Notice

Make sure your computer and phone are connected to the same Wi-Fi network to make the application work properly.



